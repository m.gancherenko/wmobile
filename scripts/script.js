

let desktop = "http://127.0.0.1:5500/";

window.addEventListener('resize',function(){
    
    const screenWidth = window.screen.width
    const screenHeight = window.screen.height

        if(screenWidth > 900){
        }
        });


let mobile = document.getElementById("root");



if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {

    mobile = document.getElementById("root");

} else {
    mobile = document.getElementById("root");

}



mobile.innerHTML = `
<body>
    <header class="header">
        <section class="logo relative">
            <img src="./images/logo.png" alt="logotype">
        </section>
        
          <div class="menu__btn"  >
                <span class="dot"></span>
                <span class="dot"></span>
                <span class="dot"></span>
        </div>
        
         <section class="menu fixed">
        <ul>
            <li><a href="#0">Головна</a></li>
            <li><a href="#main">Тарифи</a></li>
            <li><a href="#offers">Акції</a></li>
            <li><a href="#pays">Поповнення</a></li>
            <li><a href="https://stat.ws.net.ua/index/main">Особистий кабінет</a></li>
            <li><a href="#contacts">Контакти</a></li>
        </ul>
        
    </section>

    </header>


<main id="main">
 
    <section class="tariff">
        <h2  class="tariff__title title">Тарифи</h2>
        <p class="subtitle">Багатоквартирні будинки </p>
        <span class="sub__description"> ( Для фізичних осіб ) </span>

        <div class="tariff__list">
            <div class="tariff__item blackout">
                <p class="tariff__item--name"><strong> Економ </strong></p>
                <p class="tariff__item--speed"> &nbsp &nbspдо <strong> 30 Мбіт/с </strong> </p>
                <p class="tariff__item--cost">100 грн/м</p>
            </div>

            <div class="tariff__item">
                <p class="tariff__item--name"><strong> Стандарт </strong></p>
                <p class="tariff__item--speed">до <strong> 50 Мбіт/с </strong> </p>
                <p class="tariff__item--cost">110 грн/м</p>
            </div>

            <div class="tariff__item blackout">
                <p class="tariff__item--name "><strong> Комфорт </strong></p>
                <p class="tariff__item--speed">до <strong> 70 Мбіт/с </strong> </p>
                <p class="tariff__item--cost">130 грн/м</p>
            </div>

            <div class="tariff__item">
                <p class="tariff__item--name"><strong> Преміум </strong></p>
                <p class="tariff__item--speed">до <strong> 100 Мбіт/с </strong> </p>
                <p class="tariff__item--cost">140 грн/м</p>
            </div>

           

        </div>
                <div class="button">
                        <a href="#contacts">Підключити</a>
                </div>

            <div class="servises">
                <h3 class="second-title">Додаткові послуги</h3>

                <div class="servises__list" id="offers">
                    <p>Виклик iнженера з обслуговування абонентiв <strong>50 грн.</strong></p>
                       <p>Відновлення пошкодженого кабелю <strong>4 грн. метр</strong></p>
                        <p>Послуга "Зовнішній IP". Вартість <strong>+30 грн/м </strong> до тарифу</p>
                </div>
            </div>

    </section>


    <section class="offers">
        <h2  class="offers__title title">Акції</h2>

            <div class="offers__items">
                <div class="item__header">
                    <h2 class="title">50 Мбіт/с за 50 гривень </h2>
                </div>

                <div class="offers__description">
                    <p><strong>Для нових абонентів</strong>, за умови внесення авансового платежу у розмірі 100 грн. </p>


                               <p><strong>Термін дії акції - два місяці з моменту підключення</strong></p>

                        <p>Після закінчення дії тарифу, 
                        абонентська плата буде зніматися 
                        за умовами <strong>тарифного плану «Економ»</strong>.</p>
                </div>

            </div> 


                        <div class="offers__items">
                            <div class="item__header">
                                <h2 class="title"> 6 місяців за ціною 5  </h2>
                            </div>

                            <div class="offers__description">
                                <p><strong>Передплачуй! </strong> </p>

                                    <p><strong>Півроку</strong> користування Інтернетом <strong>за ціною 5 місяців</strong>, 
                                    або <strong>рік</strong> - <strong>за ціною 10 місяців</strong></p>

                            </div>

                        </div> 

                         <div class="offers__items">
                            <div class="item__header">
                                <h2 class="title"> Приведи друга </h2>
                            </div>

                            <div class="offers__description description" id="pays">

                                    <p><strong>Кожен</strong> існуючий абонент, який залучив нового користувача, <strong>отримує 50 грн. на свій особовий рахунок</strong></p>

                            </div>

                        </div> 
    </section>

    <section class="pays" >

        <h2  class="pays__title title">Поповнення рахунку</h2>

        <div class="pays__items privat">

            <div class="item__header">
                <h2> Через систему 'Приват24' </h2>
            </div>

            <div class="description">

                    <p>1. Авторизуйтесь в системі <strong>"Приват24"</strong></p>
                    <p>2. Перейдіть в меню <strong>"Мої платежі"</strong></p>
                    <p>3. У пошуку введіть <strong>"Веб-сервіс"</strong> або <strong>"Вебсервіс"</strong></p>
                    <p>4. Далі потрібно ввести свій <strong>ID</strong> і завершити оплату</p>
                    <p>5. Після оплати кошти відразу зараховуються
                        на Ваш рахунок</p>

            </div>
        </div>

        <div class="pays__items terminals">

            <div class="item__header">
                <h2> Термінали самообслуговування </h2>
            </div>

            <div class="description">

                <p >Тепер Ви можете поповнювати свiй рахунок в
                    зручний для Вас час через термінали
                    самоообслуговування <strong>City24</strong>, <strong>EasyPay</strong>, <strong>IBox</strong>.</p>
                <p>Кошти миттєво зараховуються на  баланс.</p>
                <p>Для поповнення потрiбно знати номер свого
                    особового рахунку (UID), який вказаний у
                    Вашому особистому кабінеті</p>
                <p>Для детальної інструкції можете зателефонувати
                    оператору за номером <strong><a href="tel:(097) 871-79-52">(097) 871-79-52</a></strong> </p>

            </div>
        </div>

         <div class="pays__items terminal-addresses">
            <div class="item__header">
                <h2> Адреси розташувань терміналів</h2>
            </div>

            <div class="description">

              <div class="sity24">
                  <p> <strong>City24 :</strong></p>
                  <ul class="pays__list">
                      <li> вул. Вінницька 1 - ТЦ Лiмон</li>
                      <li>вул. Вінницька 18 - ТЦ Пасаж</li>
                      <li>вул. Житомирська 20/1 - АТБ</li>
                      <li>вул. Вінницька 71 - зупинка</li>
                      <li>вул. Європейська 26 - ТЦ Бальзак</li>
                      <li>вул. Європейська 80 - магазин Крамниця</li>
                      <li>вул. Привокзальна 1А - Автовокзал Б</li>
                      <li> вул. Нiзгурецька 59 - магазин</li>
                      <li>вул. Нiзгурецька 67 - магазин</li>
                      <li>вул. Червона 16 - магазин Продукти</li>
                      <li>вул. Червона 24 - магазин Наш край</li>
                      <li>вул. Білопільська 131 - магазин зупинка</li>
                      <li>вул. Одеська 58-А - АТБ</li>
                      <li>вул. Бистрицька 92б - Зупинка кiнопрокат</li>
                      <li>вул. Житомирська 113 - магазин Продукти</li>
                  </ul>
              </div>

                <div class="easypay">
                    <p><strong>EasyPay: </strong></p>

                    <ul class="pays__list">
                        <li>пл.Соборна 20 - магазин Прод Маг</li>
                        <li>вул. Житомирська 15 - магазин Allo</li>
                        <li>вул. Житомирська 38 - магазин Прод-Маг24h</li>
                        <li>вул. Житомирська 43 - магазин Квара</li>
                        <li>вул. Європейська 49a - Колобок</li>
                        <li>вул. Європейська 79 - магазин Жменя</li>
                        <li>вул. Демитрова 4 - магазин АЗС</li>
                        <li>вул. Новоіванівська 44 - магазин Продукти</li>
                        <li>вул. Вінницька 71 - магазин Полісся продукт</li>
                        <li>вул. Котовського 38/2 - магазин Червона гора</li>
                        <li>вул. Волочаєвська 37 - магазин Жменя</li>
                        <li>вул. Червонотравнева 65 - магазин Світланка</li>
                        <li>вул. 30-річчя Перемоги 30а - магазин Жменя</li>
                        <li>вул. Молодогвардійська 4 - магазин Ялинка</li>
                        <li>вул. Чуднівська 3А - магазин Жменя</li>
                        <li>вул. Войкова 96 - магазин Жменя</li>
                        <li>пл.Сєдлєцька - магазин Продуктовий</li>
                    </ul>
                </div>

                <div class="IBox">
                    <p><strong>IBox :</strong></p>

                    <ul class="pays__list">
                        <li>вул. Європейська 14 - Vodafone</li>
                        <li>вул. Європейська 26 - АТБ</li>
                        <li>вул. Одеська 40а - Траш</li>
                        <li>вул. Одеська 58 - Полісся-Продукт</li>
                        <li>вул. Житомирська 93а - WOG</li>
                        <li>вул. Житомирська 53 - Наш Край</li>
                        <li>вул. Вінницька 18 - Фора</li>
                        <li>вул. Вінницька 71 - Полісся-Продукт</li>
                        <li>вул. Грушевського 7а - Колібріс</li>
                        <li>вул. Привокзальна 1а - Фора</li>
                        <li>вул. Б. Хмельницького 1а - Наш Край</li>
                        <li>вул. Козацька 5 - Продуктова лавка</li>
                        <li>вул. Красіна 1/2 - Продовольчі товари</li>
                        <li>вул. Бистрицька 97а - Продовольчі товари</li>
                    </ul>
                </div>

            </div>
        </div>

    </section>

    <section class="contacts" id="contacts">
        <h2 class="title">Контакти</h2>

        <div class="description phones">
            <p><strong>Телефони: Viber/Telegram:</strong></p>

            <a href="tel:(097) 871-79-52">(097) 871-79-52</a>
            <a href="tel:(067) 871-79-52">(067) 871-79-52</a>
            <a href="tel:(093) 871-79-52">(093) 871-79-52</a>
        </div>


        <div class="description clocks">
            <p><strong>Графік роботи:</strong></p>

            <div class="clock">
                <span><strong>Пн - Сб:</strong> 09:00 - 18:00</span> <span> <strong>Нд:</strong> Вихідний</span>
            </div>
        </div>

        <div class="description address">
            <p><strong>Адреса:</strong></p>

           <p> м.Бердичів, вул. Героїв України, 17</p>
          <span> E-mail: <a href="mailto:ws.net.ua@gmail.com">ws.net.ua@gmail.com</a></span>

        </div>

    </section>

</main>


`;


let menuBtn = document.querySelector(".menu__btn");

let body = document.querySelector("body");


let menu = document.querySelector(".menu");

menuBtn.addEventListener('click', menuAction);

menu.addEventListener('click', menuAction);

function menuAction(){
    let dots = document.querySelectorAll('.dot');

    for (const dot of dots) {

        if(dot.getAttribute('class') !== "dot action"){
            body.classList.add('overflow')
            menu.classList.add('show')
            dot.classList.add('action')
            menu.classList.remove('hide')

        }else{
            dot.classList.remove('action')
            body.classList.remove('overflow')
            menu.classList.remove('show')
            menu.classList.add('hide')
        }

        // dot.classList.add('action')
    }
}


const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href').substr(1)
    
    document.getElementById(blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}